# libpythonpro
Módulo para exemplificar  construção de projetos no curso Pytools

Nesse curso é ensinado como contribuir com projeto de código aberto 

Link para o curso [Python Pro] (https://www.python.pro.br/)

Suportada a versão 3 de Python

Para instalar:

```console
python3 -m venv .venv
source .venv/bim/activate
pip install -r requirements-dev.txt
```
Para conferir qualidade de código:

```console
flake8

````

Tópicos a serem abordados:

1. Git
2. Virtualenv
3. Pip
